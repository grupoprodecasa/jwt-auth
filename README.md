# Weirdo Labs

JSON Web Token Autenticación para Laravel.

## Instalación en un proyecto Laravel

Ver [INSTALACIÓN](https://gitlab.com/grupoprodecasa/jwt-auth/-/blob/master/INSTALLATION.md)

## Caracteristicas

- PHP ^8.0|^8.1
- Laravel 8

## Como utilizarlo

- `git clone git@gitlab.com:grupoprodecasa/jwt-auth.git`
- `cd jwt-auth`
- `composer install`
- `./vendor/bin/phpunit`

Ver [LICENSE](https://gitlab.com/grupoprodecasa/jwt-auth/-/blob/master/LICENSE) para mas detalles.
